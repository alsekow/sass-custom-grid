var gulp = require('gulp');
var gulpif = require('gulp-if');
var connect = require("gulp-connect");
var compass = require("gulp-compass");
var sprity = require('sprity');
var gulpopen = require('gulp-open');


gulp.task('sass', function() {
    gulp.src('./src/sass/main.scss')
        .pipe(compass({
            css: 'dist/css/',
            sass: 'src/sass',
            style: 'expanded'
        }))
        .pipe(connect.reload());
});

gulp.task('sprites', function () {
    return sprity.src({
            src: './src/img/icons/*.png',
            processor: 'css',
            cssPath: '../img/',
            style: '/dist/css/sprites',
        })
        .pipe(gulpif('*.png', gulp.dest('./dist/img/'), gulp.dest('./dist/css/')))
});

gulp.task('connect', function() {
    connect.server({
        port: 3333,
        host: 'localhost',
        livereload: true
    });
});

gulp.task('open', function() {
    gulp.src('index.html')
		.pipe(gulpopen({uri: 'http://localhost:3333'}));
});

gulp.task('watch', function() {
    gulp.watch('./src/sass/*.scss', ['sprites', 'sass']);
});

gulp.task('default', ['sprites', 'sass', 'watch', 'connect', 'open']);