
##Workflow:
 * gulp as a build / task tool
 * compass to compile SASS with SCSS syntax
 * sprity to create a sprite for icons
 * simple gulp static webserver with livereload for quick development


##Features:
 * lightweight grid system (not a library)
 * no javascript
 * spriting
 * semantic HTML
 * modular, DRY CSS
 * SASS with scss syntax
 * all colors specified as variables for easier reuse
 * tested on Chrome, Firefox and IE11. Recommend testing mobile version with chrome emulator.
 * breakpoint desktop/mobile set to 768px. Tablets potrait treated as mobiles. No retina support - time constraints.



##Instructions to run.
In command line:

* npm install
* gulp

open localhost:3333/index.html



Or


Simply open index.html in the browser.




####Author: Aleksander Sekowski
####aleksander@uixlimited.com